<?php

declare(strict_types = 1);
/**
 * This file is form http://findcat.cn
 *
 * @link     http://findcat.cn
 * @email    1476982312@qq.com
 */
use Hyperf\Session\Handler;

return [
    'handler' => Handler\FileHandler::class,
    'options' => [
        'connection'     => 'default',
        'path'           => BASE_PATH . '/runtime/session',
        'gc_maxlifetime' => 1200,
        'session_name'   => 'HYPERF_SESSION_ID',
    ],
];
